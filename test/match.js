'use strict';

let should = require('chai').should(),
    match  = require('../src/gameplay/match');

describe('match', () => {
  it('should be a function', () => {
    match.should.be.a('function');
  });

  it('should require 2 parameters of equal length', () => {
    match.should.throw();
    match.bind(null, 5936).should.throw();
    match.bind(null, 28061, 593).should.throw();
  });

  it('should return 1 cow', () => {
    match(7403, 5678).should.deep.equal({ cows: 1, bulls: 0 });
  });

  it('should return 1 bull', () => {
    match(9538, 1234).should.deep.equal({ cows: 0, bulls: 1 });
  });

  it('should return 1 cow and 1 bull', () => {
    match(1273, 1756).should.deep.equal({ cows: 1, bulls: 1 });
  });

  it('should return 3 cows, 1 bulls', () => {
    match(8304, 3084).should.deep.equal({ cows: 3, bulls: 1 });
  });

  it('should return 2 cows, 2 bulls', () => {
    match(8304, 3804).should.deep.equal({ cows: 2, bulls: 2 });
  });

  it('should return 4 bulls', () => {
    match(5720, 5720).should.deep.equal({ cows: 0, bulls: 4 });
  });

});