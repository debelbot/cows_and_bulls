'use strict';

let should   = require('chai').should(),
    generate = require('../src/gameplay/generate'),
    match    = require('../src/gameplay/match'),
    makeBot  = require('../src/gameplay/bot');


describe('makeBot', () => {
  it('should be a generator function', () => {
    makeBot.should.be.a('function');
    let bot1 = makeBot();
    (typeof bot1).should.equal('object');
    bot1.should.have.property('next');
    bot1.next.should.be.a('function');
  });

  it('should guess the number after unlimited tries', () => {
    const options  = { numberLength: 4 },
          myNumber = generate(options),
          bot      = makeBot(options);

    let guess = bot.next().value;

    while (guess != myNumber) {
      guess = bot.next(match(myNumber, guess)).value;
    }

    guess.should.equal(myNumber);
  });
});