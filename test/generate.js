'use strict';
let should   = require('chai').should(),
    generate = require('../src/gameplay/generate'),
    validate = require('../src/gameplay/validate');

describe('generate', () => {
  it('should be a function', () => {
    generate.should.be.a('function');
  });

  it('should generate only valid numbers', () => {
    for (let i = 0; i < 100; i += 1) {
      validate.bind(null, {}, generate({})).should.not.throw();
    }
  });
});