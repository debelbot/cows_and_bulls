'use strict';

let should   = require('chai').should(),
    validate = require('../src/gameplay/validate');

describe('validate', () => {
  it('should be a function', () => {
    validate.should.be.a('function');
  });

  it('should require 2 parameters, the first should be an object', () => {
    validate.should.throw();
    validate.bind(null, 1).should.throw();
    validate.bind(null, {}, 1).should.throw();
  });

  it('should check for number length', () => {
    validate.bind(null, { numberLength: 1 }, 12).should.throw();
    validate.bind(null, { numberLength: 5 }, 123).should.throw();
    validate.bind(null, { numberLength: 4 }, 1234).should.not.throw();
  });

  it('should check for non-digit characters', () => {
    validate.bind(null, { numberLength: 4 }, 'ae3d').should.throw();
    validate.bind(null, { numberLength: 4 }, '5-43').should.throw();
    validate.bind(null, { numberLength: 4 }, '378+').should.throw();
    validate.bind(null, { numberLength: 4 }, '.2o3').should.throw();
  });

  it('should check for leading 0', () => {
    validate.bind(null, {}, '0531').should.throw();
    validate.bind(null, { allowLeadingZero: true }, '0873').should.not.throw();
  });

  it('should check for digit repetitions', () => {
    validate.bind(null, {}, 7371).should.throw();
    validate.bind(null, { allowRepeatingDigits: true }, 7371).should.not.throw();
  });

  it('should return the number when valid', () => {
    validate({}, 1234).should.equal(1234);
  });

});