#Cows and bulls

A NodeJS implementation of the popular code-breaking game.

## Running

PvP Server:
-------------
    npm install -g webpack
    npm install
    npm run build
    npm start

Terminal:
----------
    node src/terminal/guessComputersNumber.js
    node src/terminal/botGuessesYourNumber.js

Tests:
-------
    npm install -g mocha
    npm install --dev
    npm test