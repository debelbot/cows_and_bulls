'use strict';

let tries = 1;

const term           = require('readline').createInterface({
        input: process.stdin,
        output: process.stdout
      }),
      options        = { numberLength: 4 },
      generateNumber = require('./../gameplay/generate').bind(null, options),
      myNumber       = generateNumber(),
      validateNumber = require('./../gameplay/validate').bind(null, options),
      testNumber     = require('./../gameplay/match').bind(null, myNumber),
      totalTries     = 20,
      tryToGuess     = () => {
        if (tries < totalTries) {
          term.question('Try to guess (' + tries + '/' + totalTries + '): ', (guess) => {
            try {
              const myGuess = validateNumber(guess),
                    results = testNumber(myGuess);

              tries += 1;

              console.log(results);

              if (results.bulls == options.numberLength) {
                console.log('Great job!');
                process.exit();
              }
              else {
                setImmediate(tryToGuess);
              }
            } catch (ex) {
              console.error(ex);
              setImmediate(tryToGuess);
            }
          });
        }
        else {
          console.log('Nice try! The number was: ' + myNumber);
        }
      };

tryToGuess();
