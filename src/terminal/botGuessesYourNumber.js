'use strict';

let term      = require('readline').createInterface({
      input: process.stdin,
      output: process.stdout
    }),
    options   = { numberLength: 4 /*, allowLeadingZero: true, allowRepeatingDigits: true */ },
    generate  = require('./../gameplay/generate').bind(null, options),
    validate  = require('./../gameplay/validate').bind(null, options),
    test      = require('./../gameplay/match'),
    ai        = require('./../gameplay/bot'),
    bot       = ai(options),
    startGame = () => {
      term.question('Enter a number for the AI to guess: ', (myNumber) => {
        try {
          validate(myNumber);
        } catch (ex) {
          console.log(ex);
          setImmediate(startGame);
          return;
        }

        let cnt    = 1,
            guess  = bot.next(),
            result = test(myNumber, guess.value);

        while (result.bulls !== options.numberLength) {
          console.log(cnt, '-', guess.value, '-', result);
          guess = bot.next(result);
          if (guess.done === true) {
            break;
          }
          result = test(myNumber, guess.value);
          cnt += 1;
        }

        console.log(cnt, '- Great job! The number was ', guess.value, '-', result);
        process.exit();
      });
    };

startGame();