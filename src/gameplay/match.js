'use strict';

module.exports = function calc(number, guess) {
  if (!guess || !number) {
    throw new Error('missing parameters');
  }

  guess = guess.toString();
  number = number.toString();

  if (guess.length != number.length) {
    throw new Error('bad parameters');
  }

  if (guess === number) {
    return {cows: 0, bulls: number.length};
  }

  const members = new Set(),
        testForCows = [];

  for (let n of number) {
    members.add(n);
  }

  let bulls = 0,
      cows = 0;

  for (let i = 0; i < number.length; i += 1) {
    if (guess[i] === number[i]) {
      bulls += 1;
    } else {
      testForCows.push(guess[i]);
    }
  }

  for (let t of testForCows) {
    if (members.has(t)) {
      cows += 1;
    }
  }

  return {cows, bulls};
};