'use strict';

function generateDigit(options, digits) {
  let randomIndex = Math.floor(Math.random() * digits.length),
      newDigit    = digits[randomIndex];

  if (!options.allowRepeatingDigits) {
    digits.splice(randomIndex, 1);
  }

  return newDigit;
}

module.exports = function generate(options) {
  if (!options || typeof options != 'object') {
    throw new Error('bad options');
  }

  let newNumberString = '';
  const digits = [1, 2, 3, 4, 5, 6, 7, 8, 9];

  if (!options.allowLeadingZero) {
    newNumberString += generateDigit(options, digits);
  }

  digits.push(0);

  while (newNumberString.length < (options.numberLength || 4)) {
    newNumberString += generateDigit(options, digits);
  }

  return newNumberString;
};