'use strict';

const digitsOnly = /^\d+$/;

module.exports = function validate(options, number) {
  if (!options || !number) {
    throw new Error('missing parameters');
  }

  if (typeof options != 'object') {
    throw new Error('bad parameters');
  }

  options.numberLength = options.numberLength || 4;

  let numberString = number.toString();

  if (numberString.length !== options.numberLength) {
    throw new Error('number should contain ' + options.numberLength + ' digits');
  }

  if (!options.allowLeadingZero) {
    if (numberString[0] === '0') {
      throw new Error('number cannot start with 0');
    }
  }

  if (!digitsOnly.test(numberString)) {
    throw new Error('number should contain digits only');
  }

  if (!options.allowRepeatingDigits) {
    const members = new Set();
    for (let n of numberString) {
      if (members.has(n)) {
        throw new Error('digits in number should not repeat');
      }
      members.add(n);
    }
  }

  return number;
};