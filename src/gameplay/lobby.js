'use strict';

module.exports = function createLobby() {
  let nextPhase = null;

  const players = new Set(),
        checkReady = () => {
          if (players.size === 2 && typeof nextPhase === 'function') {
            nextPhase(players);
          }
        },
        onReady = (callback) => {
          nextPhase = callback;
          checkReady();
        },
        join    = player => {
          players.add(player);
          checkReady();
        },
        leave   = player => {
          players.delete(player);
        };

  return {
    join,
    leave,
    onReady
  };
};