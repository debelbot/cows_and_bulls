'use strict';

const testNumber = require('./match'),
      generateNumber = require('./generate'),
      generateGuesses = function* generateGuesses(options) {
        const previousGuesses = new Map();

        while (true) {
          let rejected  = false,
              nextGuess = generateNumber(options);

          if (previousGuesses.has(nextGuess)) {
            continue;
          }

          for (let entry of previousGuesses) {
            let guess      = entry[0],
                result     = entry[1],
                testResult = testNumber(nextGuess, guess);

            if (testResult.bulls < result.bulls || (result.bulls === 0 && testResult.cows < result.cows)) {
              rejected = true;
              break;
            }
          }

          if (rejected === true) {
            continue;
          }

          let result = yield nextGuess;

          previousGuesses.set(nextGuess, result);
        }
      };
    
module.exports = generateGuesses;