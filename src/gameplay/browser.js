'use strict';

const entryHash           = window.location.hash.substr(1),
      sessionHash         = entryHash || Math.floor(Math.random() * 1000000).toString(27),
      menuUI              = document.querySelector('#menu'),
      playerNameUI        = document.querySelector('#player-name'),
      myGuesses           = document.querySelector('#my-guesses'),
      opponentGuesses     = document.querySelector('#opponent-guesses'),
      myLog               = document.querySelector('#my-log'),
      inputArea           = document.querySelector('#number-area'),
      numberLabel         = document.querySelector('#number-label'),
      numberInput         = document.querySelector('#number-input'),
      numberBtn           = document.querySelector('#number-button'),
      vsPlayerBtn         = document.querySelector('#menu-vs-player'),
      rematchBtn          = document.querySelector('#menu-rematch'),
      validateNumber      = require('./validate'),
      matchNumbers        = require('./match'),
      socket              = io(),
      validateInputUI     = (event) => {
        const charCode = (event.which) ? event.which : event.keyCode;
        return !(charCode > 31 && (charCode < 48 || charCode > 57));
      },
      makePromptUI        = (options) => {
        return new Promise((y, n) => {
          toggleInputEnabled(true);
          numberInput.value = '';
          numberLabel.textContent = options.label;
          numberBtn.textContent = options.button;
          numberInput.onkeypress = options.validator;
          inputArea.style.display = 'block';
          numberBtn.onclick = () => {
            numberBtn.onclick = () => false;
            const output = numberInput.value;
            numberInput.value = '';
            y(output);
          }
        });
      },
      promptSecretUI      = makePromptUI.bind(null, {
        label: 'Enter your secret number:',
        button: 'Play',
        validator: validateInputUI
      }),
      promptGuessUI       = makePromptUI.bind(null, {
        label: 'Try to guess their number:',
        button: 'Guess',
        validator: validateInputUI
      }),
      promptNameUI        = makePromptUI.bind(null, {
        label: 'Enter your nickname:',
        button: 'Connect',
        validator: _ => true
      }),
      guessUI             = (area, data) => {
        let newRowStr = '<tr style="color:"' + (data.error ? 'red' : 'black') + '">';

        if (data.guess) {
          newRowStr += '<td>' + data.guess + '</td>';
        }

        if (data.error) {
          newRowStr += '<td colspan="2">' + data.error + '</td>';
        } else if (data.matches) {
          newRowStr += '<td>'
            + data.matches.cows
            + '</td><td>'
            + data.matches.bulls
            + '</td>';
        } else {
          newRowStr += data;
        }

        newRowStr += '</tr>';

        area.innerHTML += newRowStr;
      },
      myGuessUI           = data => guessUI(myGuesses, data),
      opponentGuessUI     = data => guessUI(opponentGuesses, data),
      logUI               = (area, data) => {
        area.innerHTML += data + '<br/>';
      },
      logMyUI             = logUI.bind(null, myLog),
      toggleInputEnabled  = (state) => {
        state = state !== undefined ? !state : !numberInput.disabled;
        numberInput.disabled = state;
        numberBtn.disabled = state;
      },
      sendGuess           = guess => {
        socket.emit('guess', guess);
        return guess;
      },
      awaitResult         = guess => {
        const key = 'result' + guess;
        return new Promise((y, n) => {
          socket.once(key, result => {
            if (result.error) {
              n(result);
              return;
            }

            y(result);
          });
        });
      },
      playTurn            = options => {
        const validateMyGuess = validateNumber.bind(null, options);
        toggleInputEnabled(true);
        promptGuessUI()
          .then(guess => {
            toggleInputEnabled(false);
            return guess;
          })
          .then(validateMyGuess)
          .then(sendGuess)
          .then(awaitResult)
          .then(myGuessUI)
          .catch(error => {
            myGuessUI({ guess: '----', error });
            setTimeout(playTurn.bind(null, options), 0);
          });
      },
      handleOpponentGuess = (options, secret, guess) => {
        try {
          validateNumber(options, guess);
          const matches = { guess, matches: matchNumbers(secret, guess) };
          opponentGuessUI(matches);
          socket.emit('result' + guess, matches);
        } catch (ex) {
          logMyUI({ guess, error: ex.message });
        }
      },
      promptSecret        = (options) => {
        return promptSecretUI()
          .then(secretNumber => {
            validateNumber(options, secretNumber);
            opponentGuessUI({ guess: secretNumber, error: 'Your secret number' });
            socket.on('turn', playTurn.bind(null, options));
            socket.on('guess', handleOpponentGuess.bind(null, options, secretNumber));
            inputArea.style.display = 'none';
            return secretNumber;
          });
      },
      startGame           = options => {
        menuUI.style.display = 'none';
        myGuesses.innerHTML = '';
        opponentGuesses.innerHTML = '';
        myLog.innerHTML = '';
        socket.emit('start', options);
        promptSecret(options).then(secret => {
            socket.once('exception', logMyUI);
            socket.once('opponentReady', () => {
              logMyUI('Opponent has connected.');
            });
            socket.emit('numberReady');
            if (!options.rematch) {
              window.location.hash = sessionHash;
              logMyUI('Share the url with your friend to play.');
            }
          })
          .catch(error => {
            logMyUI(error);
            setTimeout(startGame.bind(null, options), 0);
          });
      },
      joinGame            = (options) => {
        if (!options.gameId) {
          menuUI.style.display = 'block';
          return;
        }

        myGuesses.innerHTML = '';
        opponentGuesses.innerHTML = '';
        myLog.innerHTML = '';
        menuUI.style.display = 'none';
        socket.emit('join', options);
        socket.once('exception', error => {
          logMyUI(options.gameId + ' ' + error);
          window.location.hash = '';
          menuUI.style.display = 'block';
        });
        socket.once('joined', options => {
          promptSecret(options)
            .then(secret => socket.emit('numberReady'))
            .catch(error => {
              logMyUI(error);
              setTimeout(joinGame.bind(null, options), 0);
            });
        });
      },
      promptName          = () => {
        return promptNameUI().then(name => {
          playerNameUI.innerText = name;
          inputArea.style.display = 'none';
          menuUI.style.display = 'block';
        }).catch(() => {
          setTimeout(promptName, 0);
        });
      };

vsPlayerBtn.onclick = () => {
  startGame({ numberLength: 4, gameId: sessionHash });
  return false;
};

rematchBtn.onclick = () => {
  rematchBtn.style.display = 'none';
  socket.once('rematch', isHost => {
    if (isHost) {
      startGame({ rematch: true, gameId: sessionHash });
    }
    else {
      joinGame({ rematch: true, gameId: sessionHash });
    }
  });
  socket.emit('rematch', { gameId: sessionHash });
  return false;
};

socket.on('end', (results) => {
  toggleInputEnabled(false);
  socket.removeAllListeners('turn');
  socket.removeAllListeners('guess');
  menuUI.style.display = 'block';
  myGuessUI(
    '<h5>You ' + (results.winner ? 'win' : 'lose')
    + (results.error ? '. ' + results.error : '')
    + '<h5>'
  );
  rematchBtn.style.display = 'inline-block';
});

socket.on('exception', logMyUI);

joinGame({ gameId: entryHash });