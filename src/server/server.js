'use strict';

const port               = 4321,
      express            = require('express'),
      app                = express(),
      http               = require('http').createServer(app),
      sio                = require('socket.io')(http),
      generateNumber     = require('../gameplay/generate'),
      validateNumber     = require('../gameplay/validate'),
      matchNumbers       = require('../gameplay/match'),
      makeBot            = require('../gameplay/bot'),
      rematchHosted      = new Set(),
      opponentsOnConnect = new Map(),
      connectedPlayers   = new WeakMap(),
      coinToss = () => Math.random() < 0.5,
      onJoinGame         = (player, options) => {
        const connect = opponentsOnConnect.get(options.gameId);

        if (typeof connect !== 'function') {
          player.emit('exception', 'game not found');
          return;
        }

        connect(player);
      },
      awaitOpponent      = (gameId) => {
        return new Promise((y, n) => {
          opponentsOnConnect.set(gameId, opponent => {
            opponentsOnConnect.delete(gameId);
            y(opponent);
          });
        });
      },
      onStartGame = (host, options) => {
        if (connectedPlayers.has(host)) {
          host.emit('exception', 'You are already in a game');
        }

        let hostReady = false,
            bothReady = () => {
              const opponent = connectedPlayers.get(host);
              if (!hostReady || !opponent) return;
              (coinToss() ? host : opponent).emit('turn');
            };

        host.once('numberReady', () => {hostReady = true; bothReady()});

        awaitOpponent(options.gameId).then(opponent => {
          connectedPlayers.set(host, opponent);
          connectedPlayers.set(opponent, host);

          rematchHosted.delete(options.gameId);
          opponent.once('numberReady', bothReady);

          host.emit('opponentReady');
          opponent.emit('joined', options);
        });
      },
      onGuess = (player, guess) => {
        if (!connectedPlayers.has(player)) {
          player.emit('result' + guess, { error: 'Start a game and then start guessing' });
          return;
        }

        const opponent = connectedPlayers.get(player);
        opponent.emit('guess', guess);
        opponent.once('result' + guess, matches => {
          player.emit('result' + guess, matches);
          if (matches.matches.bulls === 4) {
            player.emit('end', { winner: true });
            opponent.emit('end', { winner: false });
            connectedPlayers.delete(player);
            connectedPlayers.delete(opponent);
          } else {
            opponent.emit('turn');
          }
        });
      };

sio.on('connection', (socket) => {
  socket.on('disconnect', () => {
    const opponent = connectedPlayers.get(socket);
    if (opponent) {
      opponent.emit('end', { winner: 1, error: 'opponent left' });
      connectedPlayers.delete(socket);
    }
  });

  socket.on('start', onStartGame.bind(null, socket));

  socket.on('join', onJoinGame.bind(null, socket));

  socket.on('guess', onGuess.bind(null, socket));

  socket.on('rematch', (options) => {
    socket.emit('rematch', !rematchHosted.has(options.gameId));
    rematchHosted.add(options.gameId);
  });
  
});

app.use(express.static(__dirname + '/../browser'));

http.listen(port);